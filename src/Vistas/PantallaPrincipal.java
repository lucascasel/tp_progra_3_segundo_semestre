package Vistas;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;

import ImagenFondo.ImagenesParaBotones;
import Negocio.Juego;

import javax.swing.JTextPane;

public class PantallaPrincipal {
	private JFrame frame = new JFrame();
	private Juego juego;
	// label
	private JTextPane tableroAnterior = new JTextPane();
	private JLabel lblTurno = new JLabel("");
	private JLabel lblJugador_1;
	private JLabel lblJugador_2;
	private JLabel lblJugadorActual = new JLabel("Jugador:");
	private JLabel lblNombreJugadorActual = new JLabel("");

	// botones
	private List<JButton> listaBotones = new ArrayList<>(9);
	private JButton btnReiniciarJuego = new JButton("Reiniciar");

	private final static Integer ancho = 101;
	private final static Integer alto = 97;
	private final static Integer separador_Vertical = 8;
	private final static Integer separador_Horizontal = 5;
	private final JLabel lblGanador = new JLabel("");
	private ImagenesParaBotones imagenes = new ImagenesParaBotones();
	private List<ImageIcon> listaImagenes = imagenes.getImagenesBotones();
	private Integer cont = 0;
	private final JLabel lblfondoPantalla = new JLabel();

	public PantallaPrincipal(String jugador1, String jugador2) {
		this.juego = new Juego(jugador1.isEmpty() ? "Jugador 1": jugador1, jugador2.isEmpty() ? "Jugador 2": jugador2);
		initBts();
		setLayout(jugador1.isEmpty() ? "Jugador 1": jugador1, jugador2.isEmpty() ? "Jugador 2": jugador2);
		setLabels();
		setBtnS();
		setSyles();
		setListeners();
		setTurno(Juego.getTurno());
		setNombreJugadorActual(juego.jugadorActual().getNombre());
	}
	
	private void initBts() {
		for(int i = 0; i < 9; i++) {
			listaBotones.add(new JButton());
		}
	}
	
	private void setLayout(String jugador1, String jugador2) {
		frame.getContentPane().setBackground(new Color(0, 0, 0));
		frame.setBounds(ParametrosPantalla.POSX.getValor(), ParametrosPantalla.POSY.getValor(),
				ParametrosPantalla.ALTOPAN.getValor(), ParametrosPantalla.ANCHOPAN.getValor());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		// labels
		lblJugador_1 = new JLabel(jugador1 + ": " + juego.getJug1().getFicha());
		frame.getContentPane().add(lblJugador_1);
		lblJugador_2 = new JLabel(jugador2 + ": " + juego.getJug2().getFicha());
		frame.getContentPane().add(lblJugador_2);
		frame.getContentPane().add(lblTurno);
		frame.getContentPane().add(lblGanador);
		
		// btns
		for(JButton boton : listaBotones) {
			frame.getContentPane().add(boton);
		}
		frame.getContentPane().setLayout(null);

		lblJugadorActual.setFont(new Font("Tahoma", Font.PLAIN, 18));

		frame.getContentPane().add(lblJugadorActual);
		lblNombreJugadorActual.setForeground(Color.WHITE);

		frame.getContentPane().add(lblNombreJugadorActual);
		frame.getContentPane().add(tableroAnterior);
		
		
		
		frame.getContentPane().add(btnReiniciarJuego);
		lblfondoPantalla.setBounds(0, 0, 800, 600);
		
		frame.getContentPane().add(lblfondoPantalla);
	}

	private void setLabels() {
		lblJugador_1.setBounds(514, 107, 166, 51);
		lblJugador_2.setBounds(514, 267, 166, 51);
		lblTurno.setBounds(10, 419, 83, 36);
		lblGanador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGanador.setBounds(0, 511, 632, 50);
		lblJugadorActual.setBounds(514, 419, 83, 36);
		lblNombreJugadorActual.setBounds(618, 419, 100, 36);
		tableroAnterior.setBounds(215, 423, 190, 94);
		tableroAnterior.setEditable(false);
	}

	private void setBtnS() {
		Integer contador = 1;
		Integer primer_posX = 63;
		Integer primer_posY = 46;
		for(JButton boton : listaBotones) {
			boton.setBounds(primer_posX, primer_posY, ancho, alto);
			primer_posX += (ancho +separador_Vertical);
			if(contador == 3 || contador == 6) {
				primer_posY +=(alto + separador_Horizontal);
				primer_posX = 63;
			}
			contador++;
		}
		btnReiniciarJuego.setBounds(636, 511, 148, 50);
		setVisiReinicar(false);
	}

	private void setSyles() {	
		cont = 0;
		// lbls
		lblfondoPantalla.setIcon(imagenes.getFondoPantalla());
		lblTurno.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTurno.setForeground(Color.WHITE);
		lblJugador_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblJugador_1.setForeground(Color.WHITE);
		lblJugador_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblJugador_2.setForeground(Color.WHITE);
		lblJugadorActual.setForeground(Color.WHITE);
		
		lblGanador.setForeground(Color.RED);
		lblGanador.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		// bts
		for(JButton boton : listaBotones) {
			boton.setFont(new Font("Tahoma", Font.PLAIN, 24));
			boton.setIcon(listaImagenes.get(cont));
			cont++;
		}
		tableroAnterior.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tableroAnterior.setForeground(Color.WHITE);
		tableroAnterior.setOpaque(false);
	}

	private void setListeners() {
		for(int boton = 0; boton < listaBotones.size(); boton++) {
			final Integer btn = boton;
			listaBotones.get(boton).addActionListener(e -> {
				listaBotones.get(btn).setIcon((null));
				
				listaBotones.get(btn).setText(juego.jugadorActual().getFicha());
				listaBotones.get(btn).setEnabled(false);
				
				juego.cargarJugada(btn+1, juego.jugadorActual().getFicha());
				jugadaSiguiente();
			});
		}
		btnReiniciarJuego.addActionListener(e -> {reiniciarJuego();});
	}
	private void reiniciarJuego() {
		juego.reinciar_juego();
		lblGanador.setText("");
		tableroAnterior.setText("");
		clearPantalla();
	}
	
	public void finalizar() {
		enableBtns(false);
		setLblGanador(juego.jugadorActual().getNombre());
	}
	
	public void enableBtns(boolean flag) {
		for(JButton boton : listaBotones) {
			boton.setEnabled(flag);
		}
	}

	public void limpiarBotones() {
		cont = 0;
		for(JButton boton : listaBotones) {
			boton.setText("");
			boton.setIcon(listaImagenes.get(cont));
			cont++;
		}
	}
	
	public void partidaSiguiente(String tablero) {
		setLabelTableroAnterior(tablero);
		limpiarBotones();
		enableBtns(true);
	}
	
	public void jugadaSiguiente() {
		juego.siguienteTurno();
		setTurno(Juego.getTurno());
		setNombreJugadorActual(juego.jugadorActual().getNombre());
	}

	public void setTurno(Integer nroTurno) {
		lblTurno.setText("Turno: " + (nroTurno));
	}

	public void setNombreJugadorActual(String nombre) {
		lblNombreJugadorActual.setText(nombre);
	}

	public void setJugador1(String nombre) {
		juego.setNombreJugador1(nombre);
	}

	public void setJugador2(String nombre) {
		juego.setNombreJugador2(nombre);
	}

	public void setLblGanador(String ganador) {
		lblGanador.setText("Gano " + ganador + ". Felicitaciones!!!");
	}
	
	public void setFrameVisible(boolean flag) {
		this.frame.setVisible(flag);
	}
	public void setLabelTableroAnterior(String tablero) {
		tableroAnterior.setText(tablero);
	}
	public void setVisiReinicar(boolean flag) {
		this.btnReiniciarJuego.setVisible(flag);
	}
	private void clearPantalla() {
		setLayout(juego.getJug1().getNombre(), juego.getJug2().getNombre());
		setLabels();
		setBtnS();
		setSyles();
		enableBtns(true);
		setTurno(Juego.getTurno());
	}
}