package Vistas;

public enum ParametrosPantalla {
			POSX(300), POSY(90), ALTOPAN(800), ANCHOPAN(600);
	
	private Integer valor;
			
	private ParametrosPantalla(Integer valor) {
				this.valor = valor;
			}
	
	public Integer getValor() {
		return this.valor;
	}
			
}
