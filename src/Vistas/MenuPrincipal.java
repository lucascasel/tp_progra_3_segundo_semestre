package Vistas;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import ImagenFondo.ImagenesParaBotones;

import java.awt.Font;
import java.awt.Color;

public class MenuPrincipal {

	private JFrame frame = new JFrame();
	private JLabel lblTaTeTi = new JLabel();
	private JLabel lblNombreJugador = new JLabel("Nombre Jugador 1:");
	private JLabel lblNombreJugador2 = new JLabel("Nombre Jugador 2:");
	private JLabel lblFondo = new JLabel();
	private JTextField nombreJugador1 = new JTextField();
	private JTextField nombreJugador2 = new JTextField();
	private JButton btnJugar = new JButton("Jugar!!!!!!");
	private Boolean aJugar = false;
	private PantallaPrincipal ventana;
	private ImagenesParaBotones imagenes = new ImagenesParaBotones();

	public MenuPrincipal() {
		setLayout();
		setVisible(true);
		setStyle();
		setListeners();
	}
	public void setVisible(boolean flag) {
		this.frame.setVisible(flag);
	}
	
	public PantallaPrincipal getPantallaPrincial() {
		return this.ventana;
	}

	private void setLayout() {
		frame.setBounds(ParametrosPantalla.POSX.getValor(), ParametrosPantalla.POSY.getValor(),
				ParametrosPantalla.ALTOPAN.getValor(), ParametrosPantalla.ANCHOPAN.getValor());

		frame.getContentPane().setLayout(null);

		btnJugar.setBounds(0, 508, 784, 53);

		frame.getContentPane().add(btnJugar);

		lblNombreJugador.setBounds(120, 200, 189, 59);
		frame.getContentPane().add(lblNombreJugador);

		lblNombreJugador2.setBounds(120, 300, 189, 59);
		frame.getContentPane().add(lblNombreJugador2);

		nombreJugador1.setBounds(448, 210, 172, 38);
		frame.getContentPane().add(nombreJugador1);
		nombreJugador1.setColumns(10);
		nombreJugador1.setText("");

		nombreJugador2.setBounds(448, 310, 172, 38);
		frame.getContentPane().add(nombreJugador2);
		nombreJugador2.setColumns(10);
		nombreJugador2.setText("");

		lblTaTeTi.setBounds(260, 0, 266, 194);
		frame.getContentPane().add(lblTaTeTi);
		lblFondo.setBounds(0, 0, 800, 600);
		frame.getContentPane().add(lblFondo);
		lblFondo.setIcon(imagenes.getFondoPantalla());
		
		
		

	}

	private void setStyle() {
		frame.getContentPane().setBackground(new Color(0, 0, 0));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		btnJugar.setEnabled(true);
		lblTaTeTi.setFont(new Font("Consolas", Font.PLAIN, 26));
		lblTaTeTi.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreJugador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNombreJugador.setForeground(new Color(255,255, 255));
		lblNombreJugador2.setForeground(new Color(255, 255, 255));
		lblNombreJugador2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nombreJugador1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nombreJugador2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTaTeTi.setIcon(imagenes.getImagenFondo());

	}

	private void setListeners() {
		btnJugar.addActionListener(e -> {
			aJugar = true;
			frame.setVisible(false);
			ventana = new PantallaPrincipal(getJugador1().toUpperCase(), getJugador2().toUpperCase());
			ventana.setFrameVisible(true);;
		});
	}

	public boolean getAjugar() {
		return this.aJugar;
	}

	public String getJugador1() {
		return nombreJugador1.getText().trim();
	}

	public String getJugador2() {
		return nombreJugador2.getText().trim();
	}
}