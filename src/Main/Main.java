package Main;

import java.awt.EventQueue;

import Vistas.MenuPrincipal;

public class Main {
	private static MenuPrincipal ventanaInicio;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventanaInicio = new MenuPrincipal();
					ventanaInicio.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
