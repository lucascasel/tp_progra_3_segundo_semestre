package ImagenFondo;


import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import Vistas.MenuPrincipal;
import Vistas.PantallaPrincipal;

public class ImagenesParaBotones {
	private List<ImageIcon> listaBotones = new ArrayList<>();
	private ImageIcon imagenfondo;
	private ImageIcon fondoPantalla;
	
	public ImagenesParaBotones() {
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_1.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_2.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_3.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_4.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_5.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_6.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_7.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_8.jpg")));
		listaBotones.add(new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/boton_9.jpg")));
		imagenfondo = new ImageIcon(PantallaPrincipal.class.getResource("/ImagenFondo/tateti.png"));
		fondoPantalla = new ImageIcon(MenuPrincipal.class.getResource("/ImagenFondo/fondo.jpg"));
	}
	
	public List<ImageIcon> getImagenesBotones(){
		return listaBotones;
	}
	
	public ImageIcon getImagenFondo() {
		return imagenfondo;
	}
	public ImageIcon getFondoPantalla() {
		return fondoPantalla;
	}

}
