package Negocio;

public enum Fichas {
	EQUIS("X"), CIRCULO("O");

	private final String ficha;

	private Fichas(String ficha) {
		this.ficha = ficha;
	}

	public String getFicha() {
		return this.ficha;
	}
}