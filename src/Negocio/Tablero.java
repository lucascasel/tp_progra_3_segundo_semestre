package Negocio;

import java.util.HashMap;
import java.util.Map;

public class Tablero {	
	private Map<Integer, String> mapa;
	private Integer posFinal = 9;

	public Tablero() {
		this.mapa = new HashMap<Integer, String>();
		for(int i = 1; i <= posFinal; i++)
			this.mapa.put(i, "");
	}

	public String getValorUbicacion(Integer ubicacion) {
		String valor = mapa.get(ubicacion);
		return valor;
	}

	public void setValorUbicacion(Integer ubicacion, String valor) {
		this.mapa.replace(ubicacion, valor);
	}

	public Integer getSize() {
		return mapa.size();
	}
	
	
	@Override
	public String toString() {
		StringBuilder cadena = new StringBuilder();
		String separador = "─┼─┼─\n";
		cadena.append(mapa.get(1) +"│" + mapa.get(2) + "│"+ mapa.get(3)+"\n");
		cadena.append(separador);
		cadena.append(mapa.get(4) +"│" + mapa.get(5) + "│"+ mapa.get(6)+"\n");
		cadena.append(separador);
		cadena.append(mapa.get(7) +"│" + mapa.get(8) + "│"+ mapa.get(9));
		return cadena.toString();
	}
}