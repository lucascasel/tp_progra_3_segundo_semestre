package Negocio;

import java.util.ArrayList;
import java.util.List;

import Vistas.MenuPrincipal;

public class Juego {
	private static MenuPrincipal ventanaInicio;
	private static Integer turno = 0;
	private boolean ganador = false;
	
	private Jugador jug1;
	private Jugador jug2;
	
	private Tablero tablero = new Tablero();
	private List<Tablero> listaDeTableros = new ArrayList<>();
	private Validador validador = new Validador();


	
	public Juego(String jugador1, String jugador2) {
		int random = (int) (Math.random() * 2) + 1;
		if (random == 1) {
			jug1 = new Jugador(Fichas.EQUIS, jugador1);
			jug2 = new Jugador(Fichas.CIRCULO, jugador2);
		} else {
			jug1 = new Jugador(Fichas.CIRCULO, jugador1);
			jug2 = new Jugador(Fichas.EQUIS, jugador2);
		}
	}

	public Jugador jugadorActual() {
		if (turno % 2 == 0)
			return jug1;
		return jug2;
	}
	
	public void cargarJugada(Integer ubicacion, String letra) {
		tablero.setValorUbicacion(ubicacion, letra);
		if(victoria(letra)) {
			ventanaInicio.getPantallaPrincial().finalizar();
			ventanaInicio.getPantallaPrincial().setVisiReinicar(true);
			ganador = true;
		}
	}
	
	public boolean victoria(String letra) {
		return validador.comprobarGanandor(letra, tablero, listaDeTableros);
	}
	
	public void siguienteTurno() {
		turno++;
		if(turno % 9 == 0 && turno != 0 && !ganador) {
			listaDeTableros.add(tablero);
			ventanaInicio.getPantallaPrincial().partidaSiguiente(tablero.toString());
			tablero = new Tablero();
		}
	}
	
	public static Integer getTurno() {
		return turno;
	}
	
	public Jugador getJug1() {
		return jug1;
	}

	public Jugador getJug2() {
		return jug2;
	}
	
	public void setNombreJugador1(String nombre) {
		if(!nombre.trim().isEmpty()) {
			jug1.setNombre(nombre);
		}
	}
	
	public void setNombreJugador2(String nombre) {
		if(!nombre.trim().isEmpty()) {
			jug2.setNombre(nombre);
		}
	}
	public void reinciar_juego() {
		turno = 0;
		ganador = false;
		listaDeTableros.clear();
		tablero = new Tablero();
	}
}