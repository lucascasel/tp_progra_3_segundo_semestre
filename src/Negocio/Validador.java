package Negocio;

import java.util.List;

public class Validador {
	private int matrix[][] = {
			//horizonales
			{1,2,3},
			{4,5,6},
			{7,8,9},
			//verticales
			{1,4,7},
			{2,5,8},
			{3,6,9},
			//diagonales
			{1,5,9},
			{7,5,3},
	};
	
	public Validador() {}

	public boolean comprobarGanandor(String ficha, Tablero tablero, List<Tablero> listaDeTableros){
		for (int i = 0; i < matrix.length; i++) {
			if(tablero.getValorUbicacion(matrix[i][0]).equals(ficha)
				&& tablero.getValorUbicacion(matrix[i][1]).equals(ficha)
					&& tablero.getValorUbicacion(matrix[i][2]).equals(ficha)) 
				return true;		
		}
		return comprobarToroidal(ficha, tablero, listaDeTableros);
	}	
	
	private boolean comprobarToroidal(String ficha, Tablero tablero, List<Tablero> listaDeTableros){
		if (listaDeTableros != null && listaDeTableros.size() > 0) {
			Tablero anterior = listaDeTableros.get(listaDeTableros.size() - 1);
			
			if(anterior.getValorUbicacion(3).equals(ficha)
				&& tablero.getValorUbicacion(4).equals(ficha)
					&& tablero.getValorUbicacion(8).equals(ficha))
				return true;
			
			if(anterior.getValorUbicacion(9).equals(ficha)
					&& tablero.getValorUbicacion(4).equals(ficha)
						&& tablero.getValorUbicacion(2).equals(ficha))
					return true;
			
			if(anterior.getValorUbicacion(8).equals(ficha)
					&& anterior.getValorUbicacion(6).equals(ficha)
						&& tablero.getValorUbicacion(1).equals(ficha))
					return true;
			
			if(anterior.getValorUbicacion(2).equals(ficha)
					&& anterior.getValorUbicacion(6).equals(ficha)
						&& tablero.getValorUbicacion(7).equals(ficha))
					return true;
		}
		return false;	
	}
}