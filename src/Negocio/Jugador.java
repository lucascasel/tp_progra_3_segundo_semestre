package Negocio;

public class Jugador {
	private Fichas ficha;
	private String nombre;
	
	public Jugador(Fichas f, String nom){
		this.ficha = f;
		this.nombre = nom;
	}
	
	public String getFicha() {
		return ficha.getFicha();
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}